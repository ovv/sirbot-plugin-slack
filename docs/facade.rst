======
Facade
======

The facade regroup method accessible to other plugins

.. literalinclude:: ../sirbot_plugin_slack/facade.py