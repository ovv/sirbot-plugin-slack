.. sir-bot-a-lot documentation master file, created by
   sphinx-quickstart on Tue Jul  9 22:26:36 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================
Welcome to sirbot-plugin-slack's documentation!
===============================================

A slack plugin for `Sir-bot-a-lot`_


`Sir-bot-a-lot`_ is a bot built for the people and by the people of the `python developers slack community`_.
Want to join? `Get an invite`_ !

.. toctree::
   :maxdepth: 2

   quickstart
   installation
   usage
   plugins
   facade
   message
   contributing
   history
   authors
   license

.. _Sir-bot-a-lot: http://sir-bot-a-lot.readthedocs.io/en/latest/
.. _Get an invite: http://pythondevelopers.herokuapp.com/
.. _python developers slack community: https://pythondev.slack.com/
